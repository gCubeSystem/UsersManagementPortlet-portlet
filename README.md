# gCube System - Users Management Portlet

This component is a Liferay 6.2.6 CE Porlet providing the functionality for managing the available users and groups for each VO/VRE. This portlet is meant to be accessed by users with the "VO-Admin" role at VO level and with the "VRE-Manager" role at VRE level

## Structure of the project

* The source code is present in the src folder. 

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

* User's Guide: https://gcube.wiki.gcube-system.org/gcube/Users%27_Management

## Change log

See [Releases](https://code-repo.d4science.org/gCubeSystem/UsersManagementPortlet/releases).

## Authors

* **Panagiota Koltsida** - [National and Kapodistrian University of Athens & Athena' Research Center)
* **Vassilis Floros** - [Communication & Information Technologies Experts S.A - CITE (Greece)]
* **Massimiliano Assante** - [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)

## Maintainers

* **Massimiliano Assante** - [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes including:

- the Sixth Framework Programme for Research and Technological Development
    - DILIGENT (grant no. 004260);
- the Seventh Framework Programme for research, technological development and demonstration 
    - D4Science (grant no. 212488), D4Science-II (grant no.239019), ENVRI (grant no. 283465), EUBrazilOpenBio (grant no. 288754), iMarine(grant no. 283644);
- the H2020 research and innovation programme 
    - BlueBRIDGE (grant no. 675680), EGIEngage (grant no. 654142), ENVRIplus (grant no. 654182), Parthenos (grant no. 654119), SoBigData (grant no. 654024),DESIRA (grant no. 818194), ARIADNEplus (grant no. 823914), RISIS2 (grant no. 824091), PerformFish (grant no. 727610), AGINFRAplus (grant no. 731001);
    