
# Changelog for VRE Deploy Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v4.0.0] - 2021-05-13

- Feature #21269: make roles either assigned or removed in different operations, not mixed.
- Ported to git, see other changelog.xml for more changelogs.

## [v1.0.0] - 2016-06-20

- First release of the redesigned portle